from django.shortcuts import render
from django.views.generic import ListView, DetailView, DeleteView, CreateView
# from .forms import MatkulForm
from .models import Matkul, Kegiatan, Peserta
from django.urls import reverse_lazy

# Create your views here.

def index(request):
    return render(request, 'home/index.html')

def about(request):
    return render(request, 'home/about.html')

def project(request):
    return render(request, 'home/project.html')

def accordion(request):
    return render(request, 'home/accordion.html')

class MatkulView(ListView):
    model = Matkul
    template_name = 'home/list.html'

class MatkulDetailView(DetailView):
    model = Matkul
    template_name = 'home/detail.html'

class MatkulDeleteView(DeleteView):
    model = Matkul
    template_name = 'home/delete.html'
    success_url = reverse_lazy('home:list')

class MatkulCreateView(CreateView):
    model = Matkul
    template_name = "home/matkul_create.html"
    fields = '__all__'

# def Matkul_create_view(request):
#     form = MatkulForm(request.POST or None)
#     if form.is_valid():
#         form.save()
#         form = MatkulForm()
#     context = {
#         "form": form
#     }
#     return render(request, 'home/matkul_create.html', context)

class ListKegiatanView(ListView):

    context_object_name = "list_kegiatan"
    queryset = Kegiatan.objects.all()
    template_name='home/kegiatan.html'

    def get_context_data(self, **kwargs):
        context = super(ListKegiatanView, self).get_context_data(**kwargs)
        context['orang'] = Peserta.objects.all()
        return context

class AddKegiatanView(CreateView):
    model = Kegiatan
    template_name = 'home/add_kegiatan.html'
    fields = '__all__'

class AddPesertaView(CreateView):
    model = Peserta
    template_name = 'home/add_peserta.html'
    fields = '__all__'